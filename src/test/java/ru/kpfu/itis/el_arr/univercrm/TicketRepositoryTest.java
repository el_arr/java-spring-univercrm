package ru.kpfu.itis.el_arr.univercrm;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import ru.kpfu.itis.el_arr.univercrm.model.Ticket;
import ru.kpfu.itis.el_arr.univercrm.repository.TicketRepository;

import java.util.ArrayList;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = UniverCRMApplication.class)
@DataJpaTest
public class TicketRepositoryTest {
    @Autowired
    private TicketRepository ticketRepository;

    @Autowired
    private TestEntityManager testEntityManager;

    @Test
    public void whenFindById_thenReturnTicketOptional() {
        Ticket ticket = Ticket.builder().build();
        testEntityManager.persist(ticket);
        testEntityManager.flush();

        Optional<Ticket> found = ticketRepository.findById(ticket.getId());

        assertThat(found.isPresent()).isEqualTo(true);
    }

    @Test
    public void whenGetAllTickets_thenReturnAllTickets() {
        Ticket ticket1 = Ticket.builder().id(1L).build();
        Ticket ticket2 = Ticket.builder().id(2L).build();
        ArrayList<Ticket> tickets = new ArrayList<>();
        tickets.add(ticket1);
        tickets.add(ticket2);
        Mockito.when(ticketRepository.findAll()).thenReturn(tickets);

        assertThat(ticketRepository.findAll().size()).isEqualTo(2);
    }
}
