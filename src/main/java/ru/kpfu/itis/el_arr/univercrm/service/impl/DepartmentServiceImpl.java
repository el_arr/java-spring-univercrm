package ru.kpfu.itis.el_arr.univercrm.service.impl;

import org.springframework.stereotype.Service;
import ru.kpfu.itis.el_arr.univercrm.model.Department;
import ru.kpfu.itis.el_arr.univercrm.repository.DepartmentRepository;
import ru.kpfu.itis.el_arr.univercrm.service.DepartmentService;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class DepartmentServiceImpl implements DepartmentService {
    private DepartmentRepository departmentRepository;

    public DepartmentServiceImpl(DepartmentRepository departmentRepository) {
        this.departmentRepository = departmentRepository;
    }

    @Override
    public Department addDepartment(String email, String phone_num, String description) {
        Department department = Department.builder()
                .email(email)
                .phone_num(phone_num)
                .description(description)
                .build();
        departmentRepository.save(
                department
        );
        return department;
    }

    @Override
    public void removeDepartmentById(long id) {
        departmentRepository.deleteById(id);
    }

    @Override
    public List<Department> getAllDepartments() {
        return departmentRepository.findAll();
    }

    @Override
    public List<Department> getDepartmentsByNamePart(String q) {
        return departmentRepository.findAll()
                .stream()
                .filter(o -> o.getName().toLowerCase()
                        .contains(q.toLowerCase())
                ).collect(Collectors.toList());
    }

    @Override
    public Department getDepartmentById(long id) {
        Optional<Department> storeOptional = departmentRepository.findById(id);
        return storeOptional.orElse(null);
    }

    @Override
    public Department getDepartmentByName(String name) {
        return departmentRepository.findAll()
                .stream()
                .filter(o -> o.getName().toLowerCase()
                        .contains(name.toLowerCase())
                ).findAny().orElse(null);
    }

}
