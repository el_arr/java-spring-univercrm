package ru.kpfu.itis.el_arr.univercrm.service;

import ru.kpfu.itis.el_arr.univercrm.model.Employee;
import ru.kpfu.itis.el_arr.univercrm.model.Enrollee;
import ru.kpfu.itis.el_arr.univercrm.model.Ticket;

import java.util.List;

public interface TicketService {

    void addTicket(Enrollee enrollee, Employee employee,
                   String title, String content,
                   String answer, String status);

    void removeTicketById(long id);

    void editTicket(long id, String title, String content,
                    String answer, String status);

    List<Ticket> getAllTickets();

    Ticket getTicketById(long id);

    Ticket getTicket(Enrollee enrollee, Employee employee);

}
