package ru.kpfu.itis.el_arr.univercrm.controller;

import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import ru.kpfu.itis.el_arr.univercrm.model.Speciality;
import ru.kpfu.itis.el_arr.univercrm.service.SpecialityService;
import ru.kpfu.itis.el_arr.univercrm.utility.PdfGenerator;

import java.io.IOException;
import java.net.InetAddress;

@Controller
public class SpecialityPageController {
    private SpecialityService specialityService;

    public SpecialityPageController(SpecialityService specialityService) {
        this.specialityService = specialityService;
    }

    @RequestMapping(value = "/speciality/{id}", method = RequestMethod.GET)
    public String showSingleSpeciality(@PathVariable("id") String id, @ModelAttribute("model") ModelMap model, Authentication authentication) {
        Speciality speciality = specialityService.getSpecialityById(Long.valueOf(id));
        model.addAttribute("speciality", speciality);
        model.addAttribute("user", authentication != null);
        return "speciality";
    }

    @RequestMapping(value = "/speciality/{id}/download", method = RequestMethod.GET)
    public String downloadList(@PathVariable("id") String id) throws IOException, InterruptedException {
        Speciality speciality = specialityService.getSpecialityById(Long.valueOf(id));
        PdfGenerator.generateList(
                InetAddress.getLocalHost().getHostAddress() + ":8080/speciality/" + id
        ).saveAs(
                "C:/Users/El_Arr/Downloads/2ка.Специальность." + speciality.getName() + ".pdf"
        );
        return "redirect:/speciality/{id}";
    }
}
