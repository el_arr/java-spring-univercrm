package ru.kpfu.itis.el_arr.univercrm.security.details;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import ru.kpfu.itis.el_arr.univercrm.model.UserLoginData;
import ru.kpfu.itis.el_arr.univercrm.model.constant.UserStatus;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class UserDetailsImpl implements UserDetails {
    private UserLoginData userLoginData;

    UserDetailsImpl(UserLoginData userLoginData) {
        this.userLoginData = userLoginData;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        GrantedAuthority authority1 = new SimpleGrantedAuthority(userLoginData.getRole().toString());
        GrantedAuthority authority2 = new SimpleGrantedAuthority(userLoginData.getStatus().toString());
        List<GrantedAuthority> authorities = new ArrayList<>();
        authorities.add(authority1);
        authorities.add(authority2);
        return authorities;
    }

    @Override
    public String getPassword() {
        return userLoginData.getPasswordHashed();
    }

    @Override
    public String getUsername() {
        return userLoginData.getUsername();
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return userLoginData.getStatus().equals(UserStatus.CONFIRMED);
    }

}
