package ru.kpfu.itis.el_arr.univercrm.model.constant;

import java.util.ArrayList;
import java.util.List;

public enum TicketStatus {
    OPEN,
    PROCESSING,
    ACCEPTED,
    REJECTED;

    public static List<TicketStatus> getAllStatuses() {
        List<TicketStatus> result = new ArrayList<>();
        result.add(OPEN);
        result.add(PROCESSING);
        result.add(ACCEPTED);
        result.add(REJECTED);
        return result;
    }
}
