package ru.kpfu.itis.el_arr.univercrm.controller;

import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import ru.kpfu.itis.el_arr.univercrm.service.EventService;

@Controller
public class EventPageController {
    private EventService eventService;

    public EventPageController(EventService eventService) {
        this.eventService = eventService;
    }

    @RequestMapping(value = "/event/{id}", method = RequestMethod.GET)
    public String showSingleDepartment(@PathVariable String id, @ModelAttribute("model") ModelMap model, Authentication authentication) {
        model.addAttribute("event", eventService.getEventById(Long.valueOf(id)));
        model.addAttribute("user", authentication != null);
        return "event";
    }

}
