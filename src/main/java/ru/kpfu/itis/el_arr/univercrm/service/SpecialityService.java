package ru.kpfu.itis.el_arr.univercrm.service;

import ru.kpfu.itis.el_arr.univercrm.model.Speciality;

import java.util.List;

public interface SpecialityService {

    void addSpeciality(long dept_id, String main_description,
                       String small_description_1, String small_description_2);

    void removeSpecialityById(long id);

    List<Speciality> getAllSpecialities();

    List<Speciality> getAllSpecialitiesByDepartmentId(long id);

    List<Speciality> getSpecialitiesByNamePartAndDepartmentId(String q, long id);

    Speciality getSpecialityById(long id);

}
