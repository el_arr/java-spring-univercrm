package ru.kpfu.itis.el_arr.univercrm;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@EnableJpaRepositories(basePackages = "ru.kpfu.itis.el_arr.univercrm.repository")
@EntityScan(basePackages = {"ru.kpfu.itis.el_arr.univercrm.model"})
public class UniverCRMApplication {

    public static void main(String[] args) {
        SpringApplication.run(UniverCRMApplication.class, args);
    }

}
