package ru.kpfu.itis.el_arr.univercrm.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import ru.kpfu.itis.el_arr.univercrm.service.DepartmentService;
import ru.kpfu.itis.el_arr.univercrm.service.EmployeeService;
import ru.kpfu.itis.el_arr.univercrm.service.EventService;
import ru.kpfu.itis.el_arr.univercrm.service.SpecialityService;

@Controller
public class DirectorPageController {
    private EventService eventService;
    private DepartmentService departmentService;
    private SpecialityService specialityService;
    private EmployeeService employeeService;

    public DirectorPageController(EventService eventService, DepartmentService departmentService,
                                  SpecialityService specialityService, EmployeeService employeeService) {
        this.eventService = eventService;
        this.departmentService = departmentService;
        this.specialityService = specialityService;
        this.employeeService = employeeService;
    }

    @RequestMapping(value = "/director", method = RequestMethod.GET)
    public String showControlPanel(@ModelAttribute("model") ModelMap model) {
        model.addAttribute("user", true);
        return "director-control";
    }

    @RequestMapping(value = "/director/events", method = RequestMethod.GET)
    public String showEventsPage(@ModelAttribute("model") ModelMap model) {
        model.addAttribute("user", true);
        model.addAttribute("events", eventService.getAllEvents());
        return "director-control-events";
    }

    @RequestMapping(value = "/director/departments", method = RequestMethod.GET)
    public String showDepartmentsPage(@ModelAttribute("model") ModelMap model) {
        model.addAttribute("user", true);
        model.addAttribute("departments", departmentService.getAllDepartments());
        return "director-control-departments";
    }

    @RequestMapping(value = "/director/specialities", method = RequestMethod.GET)
    public String showSpecialitiesPage(@ModelAttribute("model") ModelMap model) {
        model.addAttribute("user", true);
        model.addAttribute("specialities", specialityService.getAllSpecialities());
        return "director-control-specialities";
    }

    @RequestMapping(value = "/director/employees", method = RequestMethod.GET)
    public String showEmployeesPage(@ModelAttribute("model") ModelMap model) {
        model.addAttribute("user", true);
        model.addAttribute("employees", employeeService.getAllEmployees());
        return "director-control-employees";
    }

    /*@RequestMapping(value = "/director/employee/{id}/edit", method = RequestMethod.GET)
    public String editEmployee(@ModelAttribute("model") ModelMap map, @PathVariable String id) {
        map.addAttribute("employee", employeeService.getEmployeeById(Long.valueOf(id)));
        return "employee-edit";
    }*/

    /*@RequestMapping(value = "/director/employee/{id}/edit", method = RequestMethod.POST)
    public String editSuccess(@RequestParam String job_description,
                              @RequestParam String department_id,
                              @PathVariable String id) {
        employeeService.editEmployee(Long.valueOf(id),
                departmentService.getDepartmentById(Long.valueOf(department_id)), job_description);
        return "redirect:/director/employee/" + Long.valueOf(id) + "/edit/success";
    }*/

    /*@RequestMapping(value = "/director/employee/{id}/edit/success", method = RequestMethod.GET)
    public String afterEditSuccessRedirect() {
        return "redirect:/director/employees";
    }*/

    /*@RequestMapping(value = "/director/employee/{id}/delete", method = RequestMethod.GET)
    public String deleteEmployee(@PathVariable String id) {
        employeeService.removeEmployeeById(Long.valueOf(id));
        return "redirect:/director/employees";
    }*/

    /*@RequestMapping(value = "/director/new-employee", method = RequestMethod.GET)
    public String addEmployee() {
        return "add-employee";
    }*/

    /*@RequestMapping(value = "/director/new-employee", method = RequestMethod.POST)
    public String addEmployeeSuccess(@RequestParam String firstName,
                                     @RequestParam String secondName,
                                     @RequestParam String department_id,
                                     @RequestParam String job_description) {
        employeeService.addEmployee(firstName, secondName,
                departmentService.getDepartmentById(Long.valueOf(department_id)), job_description);
        return "redirect:/director/new-employee/success";
    }*/

    /*@RequestMapping(value = "/director/new-employee/success", method = RequestMethod.GET)
    public String afterAddSuccessRedirect() {
        return "redirect:/director/employees";
    }*/

}
