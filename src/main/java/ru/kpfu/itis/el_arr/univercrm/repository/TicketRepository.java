package ru.kpfu.itis.el_arr.univercrm.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.kpfu.itis.el_arr.univercrm.model.Ticket;

public interface TicketRepository extends JpaRepository<Ticket, Long> {}
