package ru.kpfu.itis.el_arr.univercrm.model.constant;

import java.util.ArrayList;
import java.util.List;

public enum UserRole {
    ADMIN,
    DIRECTOR,
    EMPLOYEE,
    ENROLLEE;

    public static List<UserRole> getAllRoles() {
        List<UserRole> result = new ArrayList<>();
        result.add(ADMIN);
        result.add(DIRECTOR);
        result.add(EMPLOYEE);
        result.add(ENROLLEE);
        return result;
    }
}
