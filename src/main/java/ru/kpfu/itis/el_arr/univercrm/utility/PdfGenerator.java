package ru.kpfu.itis.el_arr.univercrm.utility;

import com.github.jhonnymertz.wkhtmltopdf.wrapper.Pdf;
import com.github.jhonnymertz.wkhtmltopdf.wrapper.params.Param;

public class PdfGenerator {

    public static Pdf generateList(String url) {
        Pdf pdf = new Pdf();
        pdf.addParam(new Param("--disable-external-links"), new Param("--disable-internal-links"),
                new Param("--disable-javascript"));
        pdf.addPageFromUrl(url);
        return pdf;
    }
}
