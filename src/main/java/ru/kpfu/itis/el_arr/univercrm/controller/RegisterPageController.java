package ru.kpfu.itis.el_arr.univercrm.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import ru.kpfu.itis.el_arr.univercrm.form.RegisterForm;
import ru.kpfu.itis.el_arr.univercrm.form.validator.RegisterValidator;
import ru.kpfu.itis.el_arr.univercrm.model.Employee;
import ru.kpfu.itis.el_arr.univercrm.model.Enrollee;
import ru.kpfu.itis.el_arr.univercrm.model.User;
import ru.kpfu.itis.el_arr.univercrm.model.constant.UserRole;
import ru.kpfu.itis.el_arr.univercrm.model.constant.UserStatus;
import ru.kpfu.itis.el_arr.univercrm.service.EmployeeService;
import ru.kpfu.itis.el_arr.univercrm.service.EnrolleeService;
import ru.kpfu.itis.el_arr.univercrm.service.UserInfoService;

import javax.validation.Valid;

@Controller
public class RegisterPageController {
    private RegisterValidator registerValidator;
    private UserInfoService userInfoService;
    private EmployeeService employeeService;
    private EnrolleeService enrolleeService;

    public RegisterPageController(RegisterValidator registerValidator, UserInfoService userInfoService,
                                  EmployeeService employeeService, EnrolleeService enrolleeService) {
        this.registerValidator = registerValidator;
        this.userInfoService = userInfoService;
        this.employeeService = employeeService;
        this.enrolleeService = enrolleeService;
    }

    @InitBinder("register")
    protected void initBinder(WebDataBinder webDataBinder) {
        webDataBinder.addValidators(registerValidator);
    }

    @RequestMapping(value = "/options", method = RequestMethod.GET)
    public String showSignOptionPage(@ModelAttribute("model") ModelMap model) {
        model.addAttribute("user", false);
        return "login-options";
    }

    @RequestMapping(value = "/register", method = RequestMethod.GET)
    public String showRegisterPage(@ModelAttribute("model") ModelMap model) {
        model.addAttribute("user", false);
        return "register-form-main";
    }

    @RequestMapping(value = "/register", method = RequestMethod.POST)
    public String registerNewUser(@Valid @ModelAttribute("form") RegisterForm form,
                                  BindingResult errors, RedirectAttributes attributes) {
        if (errors.hasErrors()) {
            attributes.addFlashAttribute("error", errors.getAllErrors().get(0));
            return "redirect:/register-unsuccessful";
        }
        UserRole role;
        UserStatus status;
        switch (form.getRole()) {
            case "2":
                role = UserRole.EMPLOYEE;
                status = UserStatus.BANNED;
                break;
            default:
                role = UserRole.ENROLLEE;
                status = UserStatus.UNCONFIRMED;
        }
        if (userInfoService.getUserByUsername(form.getUsername()) == null) {
            userInfoService.addUser(
                    form.getUsername(),
                    form.getPassword(),
                    form.getFirst_name(),
                    form.getSecond_name(),
                    form.getAge(),
                    form.getContact(),
                    role, status
            );
        }
        addUsingRole(form, role, userInfoService.getUserByUsername(form.getUsername()));
        return "redirect:/register-successful";
    }

    @RequestMapping(value = "/register-all", method = RequestMethod.GET)
    public String showAllRegisterPage(@ModelAttribute("model") ModelMap model) {
        model.addAttribute("user", false);
        return "register-form-all";
    }

    @RequestMapping(value = "/register-all", method = RequestMethod.POST)
    public String registerNew(@Valid @ModelAttribute("form") RegisterForm form,
                              BindingResult errors, RedirectAttributes attributes) {
        if (errors.hasErrors()) {
            attributes.addFlashAttribute("error", errors.getAllErrors().get(0));
            return "redirect:/register-unsuccessful";
        }
        UserRole role;
        UserStatus status;
        switch (form.getRole()) {
            case "2":
                role = UserRole.EMPLOYEE;
                status = UserStatus.BANNED;
                break;
            case "3":
                role = UserRole.DIRECTOR;
                status = UserStatus.BANNED;
                break;
            case "4":
                role = UserRole.ADMIN;
                status = UserStatus.CONFIRMED;
                break;
            default:
                role = UserRole.ENROLLEE;
                status = UserStatus.UNCONFIRMED;
        }
        if (userInfoService.getUserByUsername(form.getUsername()) == null) {
            userInfoService.addUser(
                    form.getUsername(),
                    form.getPassword(),
                    form.getFirst_name(),
                    form.getSecond_name(),
                    form.getAge(),
                    form.getContact(),
                    role, status
            );
        }
        addUsingRole(form, role, userInfoService.getUserByUsername(form.getUsername()));
        return "redirect:/register-successful";
    }

    private void addUsingRole(@Valid @ModelAttribute("form") RegisterForm form, UserRole role, User user) {
        Enrollee enrollee = enrolleeService.getEnrolleeByUserId(user.getId());
        Employee employee = employeeService.getEmployeeByUserId(user.getId());
        switch (role) {
            case ENROLLEE:
                if (enrollee == null)
                    enrolleeService.addEnrollee(
                            form.getFirst_name(),
                            form.getSecond_name(),
                            null
                    );
                else
                    enrollee.setUser(user);
                break;
            default:
                if (employee == null)
                    employeeService.addEmployee(
                            form.getFirst_name(),
                            form.getSecond_name(),
                            null,
                            " "
                    );
                else
                    employee.setUser(user);
                break;
        }
    }

    @RequestMapping(value = "/register-unsuccessful", method = RequestMethod.GET)
    public String registerUnsuccessful() {
        return "redirect:/register";
    }

    @RequestMapping(value = "/register-successful", method = RequestMethod.GET)
    public String registerSuccessful() {
        return "redirect:/login";
    }

}
