package ru.kpfu.itis.el_arr.univercrm.form.validator;

import org.springframework.lang.Nullable;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;
import ru.kpfu.itis.el_arr.univercrm.form.RegisterForm;
import ru.kpfu.itis.el_arr.univercrm.model.UserLoginData;
import ru.kpfu.itis.el_arr.univercrm.repository.UserLoginDataRepository;

import java.util.Optional;

@Component("register")
public class RegisterValidator implements Validator {
    private UserLoginDataRepository userLoginDataRepository;

    public RegisterValidator(UserLoginDataRepository userLoginDataRepository) {
        this.userLoginDataRepository = userLoginDataRepository;
    }

    @Override
    public boolean supports(Class<?> aClass) {
        return aClass == RegisterForm.class;
    }

    @Override
    public void validate(@Nullable Object o, Errors errors) {
        RegisterForm registerForm = (RegisterForm) o;
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "username", "NotEmpty.registerForm.userName");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "password", "NotEmpty.registerForm.password");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "firstName", "NotEmpty.registerForm.firstName");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "secondName", "NotEmpty.registerForm.secondName");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "contact", "NotEmpty.registerForm.contact");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "age", "NotEmpty.registerForm.age");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "role", "NotEmpty.registerForm.role");
        if (!errors.hasFieldErrors("username")) {
            Optional<UserLoginData> userLoginDataOptional =
                    userLoginDataRepository.findByUsername(registerForm.getUsername());
            if (userLoginDataOptional.isPresent()) {
                errors.rejectValue("username", "Duplicate.registerForm.userName");
            }
        }
    }

}
