package ru.kpfu.itis.el_arr.univercrm.service;

import ru.kpfu.itis.el_arr.univercrm.model.Department;

import java.util.List;

public interface DepartmentService {

    Department addDepartment(String email, String phone_num, String description);

    void removeDepartmentById(long id);

    List<Department> getAllDepartments();

    List<Department> getDepartmentsByNamePart(String q);

    Department getDepartmentById(long id);

    Department getDepartmentByName(String name);

}
