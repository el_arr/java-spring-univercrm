package ru.kpfu.itis.el_arr.univercrm.model.constant;

import java.util.ArrayList;
import java.util.List;

public enum UserStatus {
    UNCONFIRMED,
    CONFIRMED,
    BANNED;

    public static List<UserStatus> getAllStatuses() {
        List<UserStatus> result = new ArrayList<>();
        result.add(UNCONFIRMED);
        result.add(CONFIRMED);
        result.add(BANNED);
        return result;
    }
}
