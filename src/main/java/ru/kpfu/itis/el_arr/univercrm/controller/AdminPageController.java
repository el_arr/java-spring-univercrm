package ru.kpfu.itis.el_arr.univercrm.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import ru.kpfu.itis.el_arr.univercrm.model.Ticket;
import ru.kpfu.itis.el_arr.univercrm.model.User;
import ru.kpfu.itis.el_arr.univercrm.model.constant.TicketStatus;
import ru.kpfu.itis.el_arr.univercrm.model.constant.UserRole;
import ru.kpfu.itis.el_arr.univercrm.model.constant.UserStatus;
import ru.kpfu.itis.el_arr.univercrm.service.EmployeeService;
import ru.kpfu.itis.el_arr.univercrm.service.EnrolleeService;
import ru.kpfu.itis.el_arr.univercrm.service.TicketService;
import ru.kpfu.itis.el_arr.univercrm.service.UserInfoService;

import java.util.List;
import java.util.stream.Collectors;

@Controller
public class AdminPageController {
    private TicketService ticketService;
    private EmployeeService employeeService;
    private EnrolleeService enrolleeService;
    private UserInfoService userInfoService;

    public AdminPageController(TicketService ticketService, EmployeeService employeeService,
                               EnrolleeService enrolleeService, UserInfoService userInfoService) {
        this.ticketService = ticketService;
        this.employeeService = employeeService;
        this.enrolleeService = enrolleeService;
        this.userInfoService = userInfoService;
    }

    @RequestMapping(value = "/admin", method = RequestMethod.GET)
    public String showControlPanel(@ModelAttribute("model") ModelMap model) {
        model.addAttribute("user", true);
        return "admin-control";
    }

    @RequestMapping(value = "/admin/users", method = RequestMethod.GET)
    public String showUsersPage(@ModelAttribute("model") ModelMap model) {
        model.addAttribute("user_data",
                userInfoService.getAllUsersLoginData());
        model.addAttribute("statuses",
                UserStatus.getAllStatuses()
                        .stream()
                        .map(Enum::name)
                        .collect(Collectors.toList()));
        model.addAttribute("roles",
                UserRole.getAllRoles()
                        .stream()
                        .map(Enum::name)
                        .collect(Collectors.toList()));
        model.addAttribute("user", true);
        return "admin-control-users-form";
    }

    @RequestMapping(value = "/admin/users/edit", method = RequestMethod.POST)
    public String editUserConfirm(@RequestParam String id,
                                  @RequestParam String role,
                                  @RequestParam String status,
                                  @RequestParam String first_name,
                                  @RequestParam String second_name,
                                  @RequestParam int age,
                                  @RequestParam String contact) {
        User user = userInfoService.getUserById(Long.valueOf(id));
        List<Ticket> received = user.getTicketsReceived();
        List<Ticket> sent = user.getTicketsSent();
        userInfoService.editUser(
                Long.valueOf(id), role, status,
                first_name, second_name, age, contact,
                received, sent
        );
        return "redirect:/admin/users/edit/success";
    }

    @RequestMapping(value = "/admin/users/edit/success", method = RequestMethod.GET)
    public String editUserSuccess() {
        return "redirect:/admin/users";
    }

    @RequestMapping(value = "/admin/users/delete", method = RequestMethod.POST)
    public String deleteUser(@RequestParam String id) {
        Long lid = Long.valueOf(id);
        User user = userInfoService.getUserById(lid);
        for (Ticket t : user.getTicketsReceived()) {
            ticketService.removeTicketById(t.getId());
        }
        for (Ticket t : user.getTicketsSent()) {
            ticketService.removeTicketById(t.getId());
        }
        switch (user.getRole()) {
            case ENROLLEE:
                enrolleeService.removeEnrollee(
                        enrolleeService.getEnrolleeByUserId(lid)
                );
                break;
            default:
                employeeService.removeEmployee(
                        employeeService.getEmployeeByUserId(lid)
                );
                break;
        }
        userInfoService.removeUser(user);
        return "redirect:/admin/users/delete/success";
    }

    @RequestMapping(value = "/admin/users/delete/success", method = RequestMethod.GET)
    public String deleteUserSuccess() {
        return "redirect:/admin/users";
    }

    @RequestMapping(value = "/admin/tickets", method = RequestMethod.GET)
    public String showTicketsPage(@ModelAttribute("model") ModelMap model) {
        model.addAttribute("tickets", ticketService.getAllTickets());
        model.addAttribute("statuses",
                TicketStatus.getAllStatuses()
                        .stream()
                        .map(Enum::name)
                        .collect(Collectors.toList()));
        model.addAttribute("user", true);
        return "admin-control-ticket-form";
    }

    @RequestMapping(value = "/admin/tickets/edit", method = RequestMethod.POST)
    public String editTicketConfirm(@RequestParam String id,
                                    @RequestParam String title,
                                    @RequestParam String content,
                                    @RequestParam String answer,
                                    @RequestParam String status) {
        ticketService.editTicket(
                Long.valueOf(id), title, content, answer, status
        );
        return "redirect:/admin/tickets/edit/success";
    }

    @RequestMapping(value = "/admin/tickets/edit/success", method = RequestMethod.GET)
    public String editTicketSuccess() {
        return "redirect:/admin/tickets";
    }

    @RequestMapping(value = "/admin/tickets/delete", method = RequestMethod.POST)
    public String deleteTicket(@RequestParam String id) {
        ticketService.removeTicketById(Long.valueOf(id));
        return "redirect:/admin/tickets/delete/success";
    }

    @RequestMapping(value = "/admin/tickets/delete/success", method = RequestMethod.GET)
    public String deleteTicketSuccess() {
        return "redirect:/admin/tickets";
    }

}
