package ru.kpfu.itis.el_arr.univercrm.model;

import lombok.*;
import ru.kpfu.itis.el_arr.univercrm.model.constant.UserRole;
import ru.kpfu.itis.el_arr.univercrm.model.constant.UserStatus;

import javax.persistence.*;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder

@Entity
@Table(name = "userlogindata")
public class UserLoginData {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "user_login_seq")
    @SequenceGenerator(name = "user_login_seq", sequenceName = "SEQ_USER_LOGIN", allocationSize = 1)
    private long id;

    @Column(unique = true)
    private String username;

    private String passwordHashed;

    @OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "related_user_id")
    private User relatedUser;

    @Enumerated(EnumType.STRING)
    private UserStatus status;

    @Enumerated(EnumType.STRING)
    private UserRole role;

}
