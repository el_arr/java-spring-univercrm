package ru.kpfu.itis.el_arr.univercrm.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.kpfu.itis.el_arr.univercrm.model.User;
import ru.kpfu.itis.el_arr.univercrm.model.UserLoginData;

import java.util.Optional;

public interface UserLoginDataRepository extends JpaRepository<UserLoginData, Long> {

    Optional<UserLoginData> findById(long id);

    Optional<UserLoginData> findByUsername(String username);

    Optional<UserLoginData> findByRelatedUser(User user);

}
