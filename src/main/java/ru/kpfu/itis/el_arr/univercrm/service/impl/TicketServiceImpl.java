package ru.kpfu.itis.el_arr.univercrm.service.impl;

import org.springframework.stereotype.Service;
import ru.kpfu.itis.el_arr.univercrm.model.Employee;
import ru.kpfu.itis.el_arr.univercrm.model.Enrollee;
import ru.kpfu.itis.el_arr.univercrm.model.Ticket;
import ru.kpfu.itis.el_arr.univercrm.model.constant.TicketStatus;
import ru.kpfu.itis.el_arr.univercrm.repository.TicketRepository;
import ru.kpfu.itis.el_arr.univercrm.service.TicketService;

import java.util.Comparator;
import java.util.Date;
import java.util.List;

@Service
public class TicketServiceImpl implements TicketService {
    private TicketRepository ticketRepository;

    public TicketServiceImpl(TicketRepository ticketRepository) {
        this.ticketRepository = ticketRepository;
    }

    @Override
    public void addTicket(Enrollee enrollee, Employee employee,
                          String title, String content,
                          String answer, String status) {
        Ticket ticket = Ticket
                .builder()
                .sender(enrollee)
                .receiver(employee)
                .date(new Date())
                .title(title)
                .content(content)
                .answer(answer)
                .status(TicketStatus.valueOf(status))
                .build();
        ticketRepository.save(ticket);
    }

    @Override
    public void removeTicketById(long id) {
        ticketRepository.deleteById(id);
        ticketRepository.flush();
    }

    @Override
    public void editTicket(long id, String title, String content, String answer, String status) {
        Ticket ticket = ticketRepository.findById(id).orElse(null);
        if (ticket != null) {
            ticket.setTitle(title);
            ticket.setContent(content);
            ticket.setAnswer(answer);
            TicketStatus ticketStatus = TicketStatus.OPEN;
            if (status.equals(TicketStatus.PROCESSING.name())) {
                ticketStatus = TicketStatus.PROCESSING;
            } else if (status.equals(TicketStatus.ACCEPTED.name())) {
                ticketStatus = TicketStatus.ACCEPTED;
            } else if (status.equals(TicketStatus.REJECTED.name())) {
                ticketStatus = TicketStatus.REJECTED;
            }
            ticket.setStatus(ticketStatus);
            ticketRepository.save(ticket);
        }
    }

    @Override
    public List<Ticket> getAllTickets() {
        return ticketRepository.findAll();
    }

    @Override
    public Ticket getTicketById(long id) {
        return ticketRepository.findById(id).orElse(null);
    }

    @Override
    public Ticket getTicket(Enrollee enrollee, Employee employee) {
        return getAllTickets()
                .stream()
                .filter(o -> (o.getSender().getId() == enrollee.getId() &&
                        o.getReceiver().getId() == employee.getId())
                ).max(Comparator.comparing(Ticket::getDate))
                .orElse(null);
    }

}
