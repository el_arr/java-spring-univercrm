package ru.kpfu.itis.el_arr.univercrm.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import ru.kpfu.itis.el_arr.univercrm.form.TicketForm;
import ru.kpfu.itis.el_arr.univercrm.form.validator.NewTicketValidator;
import ru.kpfu.itis.el_arr.univercrm.model.Employee;
import ru.kpfu.itis.el_arr.univercrm.model.Enrollee;
import ru.kpfu.itis.el_arr.univercrm.model.Ticket;
import ru.kpfu.itis.el_arr.univercrm.model.constant.TicketStatus;
import ru.kpfu.itis.el_arr.univercrm.service.*;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Controller
public class TicketController {
    private DepartmentService departmentService;
    private SpecialityService specialityService;
    private UserInfoService userInfoService;
    private EnrolleeService enrolleeService;
    private EmployeeService employeeService;
    private TicketService ticketService;
    private NewTicketValidator newTicketValidator;

    public TicketController(DepartmentService departmentService, SpecialityService specialityService,
                            UserInfoService userInfoService, EnrolleeService enrolleeService, EmployeeService employeeService,
                            TicketService ticketService, NewTicketValidator newTicketValidator) {
        this.departmentService = departmentService;
        this.specialityService = specialityService;
        this.userInfoService = userInfoService;
        this.enrolleeService = enrolleeService;
        this.employeeService = employeeService;
        this.ticketService = ticketService;
        this.newTicketValidator = newTicketValidator;
    }

    @InitBinder("newTicket")
    protected void initBinder(WebDataBinder webDataBinder) {
        webDataBinder.addValidators(newTicketValidator);
    }

    @RequestMapping(value = "/ticket/enrollee/{id}", method = RequestMethod.GET)
    public String showNewTicketPage(@PathVariable String id, @ModelAttribute("model") ModelMap model) {
        Enrollee enrollee = enrolleeService.getEnrolleeById(Long.valueOf(id));
        if (enrollee != null) {
            List<Ticket> tickets = ticketService.getAllTickets()
                    .stream()
                    .filter(o -> o.getSender().getId() == enrollee.getId())
                    .collect(Collectors.toList());
            model.addAttribute("enrollee", enrollee);
            model.addAttribute("tickets", tickets);
        }
        model.addAttribute("departments", departmentService.getAllDepartments());
        model.addAttribute("specialities", specialityService.getAllSpecialities());
        model.addAttribute("user", true);
        return "enrollee-ticket-form";
    }

    @RequestMapping(value = "/ticket/enrollee/{id}", method = RequestMethod.POST)
    public String sendNewTicket(@Valid @ModelAttribute("form") TicketForm form, @PathVariable String id,
                                BindingResult errors, RedirectAttributes attributes) {
        if (errors.hasErrors()) {
            attributes.addFlashAttribute("error", errors.getAllErrors().get(0));
            return "redirect:/ticket/enrollee/" + Long.valueOf(id) + "/unsuccessful";
        }
        Enrollee enrollee = enrolleeService.getEnrolleeById(Long.valueOf(id));
        List<Employee> employees =
                employeeService.getEmployeesByDeptId(
                        departmentService.getDepartmentById(
                                Long.valueOf(form.getReceiver())
                        ).getId()
                );
        if (employees != null && employees.size() > 0) {
            Employee employee = employees.get(0);
            if (enrollee != null && employee != null) {
                if (form.getStatus().equals(""))
                    ticketService.addTicket(
                            enrollee, employee, form.getTitle(), form.getContent(), "", "OPEN"
                    );
                else
                    ticketService.editTicket(
                            Long.valueOf(form.getId()), form.getTitle(), form.getContent(), form.getAnswer(), form.getStatus()
                    );
                Ticket ticket = ticketService.getTicket(enrollee, employee);
                List<Ticket> ticketsSent = enrollee.getUser().getTicketsSent();
                if (ticketsSent == null) {
                    ticketsSent = new ArrayList<>();
                }
                ticketsSent.add(ticket);
                userInfoService.editUserTickets(
                        enrollee.getUser(), enrollee.getUser().getTicketsReceived(), ticketsSent
                );
                return "redirect:/ticket/enrollee/" + Long.valueOf(id) + "/successful";
            }
            return "redirect:/ticket/enrollee/" + Long.valueOf(id) + "/unsuccessful";
        }
        return "redirect:/ticket/enrollee/" + Long.valueOf(id) + "/unsuccessful";
    }

    @RequestMapping(value = "/ticket/enrollee/{id}/unsuccessful", method = RequestMethod.GET)
    public String redirectAfterError(@PathVariable String id) {
        return "redirect:/ticket/enrollee/" + Long.valueOf(id);
    }

    @RequestMapping(value = "/ticket/enrollee/{id}/successful", method = RequestMethod.GET)
    public String redirectAfterSuccess(@PathVariable String id) {
        return "redirect:/ticket/enrollee/" + Long.valueOf(id);
    }

    @RequestMapping(value = "/ticket/employee/{id}", method = RequestMethod.GET)
    public String showExistingTicketPage(@PathVariable String id, @ModelAttribute("model") ModelMap model) {
        Employee employee = employeeService.getEmployeeById(Long.valueOf(id));
        if (employee != null) {
            List<Ticket> tickets = employee.getUser().getTicketsReceived();
            if (tickets != null) {
                for (Ticket t : tickets) {
                    if (t.getStatus().name().equals("OPEN")) {
                        t.setStatus(TicketStatus.PROCESSING);
                    }
                }
                if (tickets.size() == 0) {
                    model.addAttribute("tickets", null);
                } else {
                    model.addAttribute("tickets", tickets);
                }
            } else {
                model.addAttribute("tickets", null);
            }
            model.addAttribute("employee", employee);
        }
        model.addAttribute("statuses",
                TicketStatus.getAllStatuses()
                        .stream()
                        .filter(o -> !o.name().equals("OPEN"))
                        .map(Enum::name)
                        .collect(Collectors.toList())
        );
        model.addAttribute("user", true);
        return "employee-ticket-form";
    }

    @RequestMapping(value = "ticket/employee/{id}", method = RequestMethod.POST)
    public String processNewTicket(@Valid @ModelAttribute("form") TicketForm form, @PathVariable String id,
                                   BindingResult errors, RedirectAttributes attributes) {
        if (errors.hasErrors()) {
            attributes.addFlashAttribute("error", errors.getAllErrors().get(0));
            return "redirect:/ticket/employee/" + Long.valueOf(id) + "/unsuccessful";
        }
        TicketStatus status = TicketStatus.PROCESSING;
        if (form.getStatus().equals(TicketStatus.ACCEPTED.name())) {
            status = TicketStatus.ACCEPTED;
        } else if (form.getStatus().equals(TicketStatus.REJECTED.name())) {
            status = TicketStatus.REJECTED;
        }
        Enrollee enrollee = enrolleeService.getEnrolleeById(Long.valueOf(form.getSender()));
        Employee employee = employeeService.getEmployeesByDeptId(
                departmentService.getDepartmentById(
                        Long.valueOf(form.getReceiver())
                ).getId()
        ).get(0);
        if (employee != null && enrollee != null) {
            Ticket ticket = Ticket.builder()
                    .id(Long.valueOf(form.getId()))
                    .sender(enrollee)
                    .receiver(employee)
                    .date(new Date())
                    .title(form.getTitle())
                    .content(form.getContent())
                    .answer(form.getAnswer())
                    .status(status)
                    .build();
            ticketService.editTicket(
                    ticket.getId(), ticket.getTitle(), ticket.getContent(), ticket.getAnswer(), ticket.getStatus().name()
            );
            List<Ticket> ticketsSent = enrollee.getUser().getTicketsSent();
            if (ticketsSent == null) {
                ticketsSent = new ArrayList<>();
            }
            ticketsSent.add(ticket);
            userInfoService.editUserTickets(enrollee.getUser(), enrollee.getUser().getTicketsReceived(), ticketsSent);
            return "/ticket/employee/" + Long.valueOf(id) + "/successful";
        }
        return "/ticket/employee/" + Long.valueOf(id) + "/unsuccessful";
    }

    @RequestMapping(value = "ticket/employee/{id}/unsuccessful", method = RequestMethod.POST)
    public String redirectAfterProcessingError(@PathVariable String id) {
        return "redirect:/ticket/employee/" + Long.valueOf(id);
    }

    @RequestMapping(value = "ticket/employee/{id}/successful", method = RequestMethod.POST)
    public String redirectAfterProcessingSuccess(@PathVariable String id) {
        return "redirect:/ticket/employee/" + Long.valueOf(id);
    }

    @RequestMapping(value = "/ticket-search", method = RequestMethod.POST)
    @ResponseBody
    public List<String> ajaxTicketSearch(@RequestParam(value = "id") String id) {
        Ticket ticket = ticketService.getTicketById(Long.valueOf(id));
        List<String> result = new ArrayList<>();
        result.add("" + ticket.getId());
        result.add(ticket.getDate().toString());
        result.add("" + ticket.getSender().getId());
        result.add(ticket.getSender().getUser().getFirstName() + " " + ticket.getSender().getUser().getSecondName());
        result.add("" + ticket.getReceiver().getDepartment().getId());
        result.add(ticket.getReceiver().getDepartment().getName());
        result.add(ticket.getTitle());
        result.add(ticket.getContent());
        result.add(ticket.getAnswer());
        result.add(ticket.getStatus().name());
        return result;
    }

    /*
    @RequestMapping(value = "/department-specialities-search", method = RequestMethod.POST)
    @ResponseBody
    public Set<List<String>> ajaxDepartmentSpecialitiesSearch(@RequestParam(value = "q") String q) {
        Department department = departmentService.getDepartmentById(Long.valueOf(q));
        List<String> ids = new ArrayList<>();
        List<String> names = new ArrayList<>();
        for (Speciality s : department.getSpecialities()) {
            ids.add("" + s.getId());
            names.add(s.getName());
        }
        Set<List<String>> result = new HashSet<>();
        result.add(ids);
        result.add(names);
        return result;
    }
    */

}
