package ru.kpfu.itis.el_arr.univercrm.service.impl;

import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import ru.kpfu.itis.el_arr.univercrm.model.Enrollee;
import ru.kpfu.itis.el_arr.univercrm.model.User;
import ru.kpfu.itis.el_arr.univercrm.repository.EnrolleeRepository;
import ru.kpfu.itis.el_arr.univercrm.service.EnrolleeService;
import ru.kpfu.itis.el_arr.univercrm.service.UserInfoService;

import java.io.IOException;
import java.util.List;

@Service
public class EnrolleeServiceImpl implements EnrolleeService {
    private EnrolleeRepository enrolleeRepository;
    private UserInfoService userInfoService;

    public EnrolleeServiceImpl(EnrolleeRepository enrolleeRepository,
                               UserInfoService userInfoService) {
        this.enrolleeRepository = enrolleeRepository;
        this.userInfoService = userInfoService;
    }

    @Override
    public void addEnrollee(String first_name, String second_name, MultipartFile file) {
        User user = userInfoService.getUserByFullName(first_name, second_name);
        Enrollee enrollee = Enrollee.builder()
                .user(user)
                .build();
        enrolleeRepository.save(enrollee);
    }

    @Override
    public void removeEnrolleeById(long id) {
        enrolleeRepository.deleteById(id);
    }

    @Override
    public void removeEnrollee(Enrollee enrollee) {
        enrolleeRepository.delete(enrollee);
    }

    @Override
    public void editEnrollee(long id, MultipartFile file) throws IOException {
        byte[] diploma = file.getBytes();
        Enrollee enrollee = enrolleeRepository.findById(id).orElse(null);
        if (enrollee != null) {
            enrollee.setDiploma(diploma);
            enrolleeRepository.save(enrollee);
        }
    }

    @Override
    public List<Enrollee> getAllEnrollees() {
        return enrolleeRepository.findAll();
    }

    @Override
    public Enrollee getEnrolleeById(long id) {
        return enrolleeRepository.findById(id).orElse(null);
    }

    @Override
    public Enrollee getEnrolleeByUserId(long id) {
        for (Enrollee e : enrolleeRepository.findAll()) {
            if (e.getUser().getId() == id) {
                return e;
            }
        }
        return null;
    }

}
