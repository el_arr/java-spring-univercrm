package ru.kpfu.itis.el_arr.univercrm.form.validator;

import org.springframework.lang.Nullable;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;
import ru.kpfu.itis.el_arr.univercrm.form.TicketForm;

@Component("newTicket")
public class NewTicketValidator implements Validator {

    public NewTicketValidator() {}

    @Override
    public void validate(@Nullable Object o, Errors errors) {
        TicketForm ticketForm = (TicketForm) o;
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "receiver", "NotEmpty.ticketForm.receiver");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "title", "NotEmpty.ticketForm.title");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "content", "NotEmpty.ticketForm.content");
    }

    @Override
    public boolean supports(Class<?> aClass) {
        return aClass == TicketForm.class;
    }
}
