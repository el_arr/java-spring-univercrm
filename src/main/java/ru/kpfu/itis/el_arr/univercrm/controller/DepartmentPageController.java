package ru.kpfu.itis.el_arr.univercrm.controller;

import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import ru.kpfu.itis.el_arr.univercrm.model.Department;
import ru.kpfu.itis.el_arr.univercrm.model.Speciality;
import ru.kpfu.itis.el_arr.univercrm.service.DepartmentService;
import ru.kpfu.itis.el_arr.univercrm.service.SpecialityService;
import ru.kpfu.itis.el_arr.univercrm.utility.PdfGenerator;

import java.io.IOException;
import java.net.InetAddress;
import java.util.ArrayList;
import java.util.List;

@Controller
public class DepartmentPageController {
    private DepartmentService departmentService;
    private SpecialityService specialityService;

    public DepartmentPageController(DepartmentService departmentService, SpecialityService specialityService) {
        this.departmentService = departmentService;
        this.specialityService = specialityService;
    }

    @RequestMapping(value = "/departments", method = RequestMethod.GET)
    public String showAllDepartments(@ModelAttribute("model") ModelMap model, Authentication authentication) {
        List<Department> departments = departmentService.getAllDepartments();
        model.addAttribute("departments", departments.size() != 0);
        model.addAttribute("user", authentication != null);
        return "departments";
    }

    @RequestMapping(value = "/departments/download", method = RequestMethod.GET)
    public String downloadDepartmentsList() throws IOException, InterruptedException {
        PdfGenerator.generateList(
                InetAddress.getLocalHost().getHostAddress() + ":8080/departments"
        ).saveAs("C:/Users/El_Arr/Downloads/2ка.Факультеты.pdf");
        return "redirect:/departments";
    }

    @RequestMapping(value = "/department/{id}", method = RequestMethod.GET)
    public String showSingleDepartment(@PathVariable String id, @ModelAttribute("model") ModelMap model, Authentication authentication) {
        model.addAttribute("department", departmentService.getDepartmentById(Long.valueOf(id)));
        model.addAttribute("specialities",
                specialityService.getAllSpecialitiesByDepartmentId(Long.valueOf(id)) != null);
        model.addAttribute("user", authentication != null);
        return "department";
    }

    @RequestMapping(value = "/department/{id}/download", method = RequestMethod.GET)
    public String downloadSpecialitiesList(@PathVariable String id) throws IOException, InterruptedException {
        PdfGenerator.generateList(
                InetAddress.getLocalHost().getHostAddress() + ":8080/department/" + id
        ).saveAs("C:/Users/El_Arr/Downloads/2ка.Факультет." +
                departmentService.getDepartmentById(Long.valueOf(id)).getName() + ".pdf");
        return "redirect:/department/{id}";
    }

    @RequestMapping(value = "/department-search", method = RequestMethod.POST)
    @ResponseBody
    public List<List<String>> ajaxDepartmentSearch(@RequestParam(value = "q") String q) {
        List<Department> departments = departmentService.getDepartmentsByNamePart(q);
        List<String> ids = new ArrayList<>();
        List<String> names = new ArrayList<>();
        List<String> descriptions = new ArrayList<>();
        List<String> emails = new ArrayList<>();
        List<String> phone_nums = new ArrayList<>();
        for (Department d : departments) {
            ids.add("" + d.getId());
            names.add(d.getName());
            descriptions.add(d.getDescription());
            emails.add(d.getEmail());
            phone_nums.add(d.getPhone_num());
        }
        List<List<String>> result = new ArrayList<>();
        result.add(ids);
        result.add(names);
        result.add(descriptions);
        result.add(emails);
        result.add(phone_nums);
        return result;
    }

    @RequestMapping(value = "/specialities-search", method = RequestMethod.POST)
    @ResponseBody
    public List<List<String>> ajaxSpecialitiesSearch(@RequestParam(value = "q") String q, @RequestParam(value = "d") String d) {
        Department department = departmentService.getDepartmentByName(d);
        List<Speciality> specialities =
                specialityService.getSpecialitiesByNamePartAndDepartmentId(q, department.getId());
        List<String> ids = new ArrayList<>();
        List<String> names = new ArrayList<>();
        List<String> main_descriptions = new ArrayList<>();
        for (Speciality s : specialities) {
            ids.add("" + s.getId());
            names.add(s.getName());
            main_descriptions.add(s.getMain_description());
        }
        List<List<String>> result = new ArrayList<>();
        result.add(ids);
        result.add(names);
        result.add(main_descriptions);
        return result;
    }

}
