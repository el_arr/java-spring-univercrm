package ru.kpfu.itis.el_arr.univercrm.form;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class TicketForm {
    private String id;
    private String sender;
    private String receiver;
    private String title;
    private String content;
    private String answer;
    private String status;
}
