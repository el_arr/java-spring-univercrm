function search_specialities() {
    $.ajax({
        data: {"q": $("#q_id").val(), "d": $("#d_id").val()},
        dataType: 'json',
        url: "/specialities-search",
        type: 'POST',
        success: function (result) {
            $("#results").html("");
            for (var i = 0; i < result[1].length; i++) {
                $("#results").append("" +
                    "<div class='row'>" +
                    "    <section class='6u 12u$(medium)'>" +
                    "        <h2>" + result[1][i] + "</h2>" +
                    "        <p><strong>" + result[1][i] + "</strong> - это " + result[2][i] + "</p>" +
                    "    </section>" +
                    "    <section class='6u$ 12u$(small)'>" +
                    "        <h2>Хотите знать больше?</h2>" +
                    "        <a href='/speciality/" + result[0][i] + "' class='button'>" + result[1][i] + "</a>" +
                    "    </section>" +
                    "</div>" +
                    "<hr class='major'/>")
            }
        },
        error: function (e) {
            console.log("error: " + e);
        }
    });
}