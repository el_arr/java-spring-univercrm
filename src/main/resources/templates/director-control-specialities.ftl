<!DOCTYPE HTML>
<html>

<head>
    <title>Специальности</title>
    <#include "common/head.ftl">
</head>

<body class="subpage">

<#include "common/header.ftl">

<section id="main" class="wrapper">
    <div class="inner">

        <header class="align-center">
            <h2>Специальности</h2>
    <#if model.specialities?size != 0>
            <p>Ниже представлены все виды факультетов, обитающих в 2ке</p>
        </header>

        <h2 id="content">Информация</h2>
        <hr class="major"/>

        <#list model.specialities as speciality>
        <p>${speciality.getMain_description()}</p>
        <div class="row">
            <div class="4u 12u$(medium)">
                <h3>Факультет</h3>
                <p>
                    <a href="/department/${speciality.getDepartment().getId()}">
                        ${speciality.getDepartment().getName()}
                    </a>
                </p>
            </div>
            <div class="4u 12u$(medium)">
                <h3>Кем вы станете?</h3>
                <p>${speciality.getSmall_description_1()}</p>
            </div>
            <div class="4u$ 12u$(medium)">
                <h3>Что будете изучать?</h3>
                <p>${speciality.getSmall_description_2()}</p>
            </div>
        </div>
        <hr class="major"/>
        </#list>
    <#else>
            <p>К сожалению, в базе данных нет видов специальностей, обитающих в 2ке</p>
        </header>
    </#if>
    </div>
</section>

<footer id="footer">
    <div class="inner">
        <#include "common/copyright.ftl">
    </div>
</footer>

<#include "common/scripts.ftl">

</body>

</html>