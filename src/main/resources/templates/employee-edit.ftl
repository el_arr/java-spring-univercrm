<!DOCTYPE HTML>
<html xmlns="http://www.w3.org/1999/html">

<head>
    <title>Сотрудник</title>
    <#include "common/head.ftl">
</head>

<body class="subpage">

<#include "common/header.ftl">

<section id="three" class="wrapper">
    <div class="inner">

        <header class="align-center">
            <h2>Имя Фамилия</h2>
            <p>Описание работы</p>
        </header>

        <div class="image round right">
            <img src="/static/img/pic02.jpg" alt="Pic 02"/>
        </div>

        <h4>Данные</h4>
        <form id="edit" method="post" action="/employee/${model.employee.getId()}/edit-successful">
            <dl>
                <dt>Доступ</dt>
                <dd>
                    <p>${model.employee.getUser().getRole()}</p>
                </dd>
                <dt>Имя</dt>
                <dd>
                    <input type="text" name="first_name" id="first_name"
                           value="${model.employee.getUser().getFirstName()}" placeholder="Имя"/>
                </dd>
                <dt>Фамилия</dt>
                <dd>
                    <input type="text" name="second_name" id="second_name"
                           value="${model.employee.getUser().getSecondName()}" placeholder="Фамилия"/>
                </dd>
                <dt>Возраст</dt>
                <dd>
                    <input type="text" name="age" id="age"
                           value="${model.employee.getUser().getAge()}" placeholder="Возраст"/>
                </dd>
                <dt>Почтовый ящик</dt>
                <dd>
                    <input type="text" name="contact" id="contact"
                           value="${model.employee.getUser().getContact()}" placeholder="Почтовый ящик"/>
                </dd>
                <dt>Описание</dt>
                <dd>
                    <div class="6u 12u$(xsmall)">
                        <input type="text" name="job_description" id="job_description"
                               value="${model.employee.getJob_description()}" placeholder="Описание"/>
                    </div>
                    <br/>
                </dd>
                <dt>Факультет</dt>
                <dd>
                    <div class="select-wrapper">
                        <select name="department_id" id="department_id" form="edit">
                            <#list model.departments as department>
                                <option value="${department.getId()}">${department.getName()}</option>
                            </#list>
                        </select>
                    </div>
                </dd>
                <br/>
                <dd>
                    <ul class="actions">
                        <li><input value="Изменить свои данные" class="button special" type="submit"></li>
                    </ul>
                </dd>
            </dl>
        </form>
    </div>
</section>

<footer id="footer">
    <div class="inner">
        <#include "common/copyright.ftl">
    </div>
</footer>

<#include "common/scripts.ftl">

</body>

</html>
