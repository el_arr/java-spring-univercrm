<!DOCTYPE HTML>
<html>

<head>
    <title>Мероприятие</title>
    <#include "common/head.ftl">
</head>

<body class="subpage">

<#include "common/header.ftl">

<section id="main" class="wrapper">
    <div class="inner">

    <header class="align-center">
        <#if model.event??>
            <h2>${model.event.getTitle()}</h2>
            <div class="row">
                <section class="6u 12u$(medium)">
                    <h2>${model.event.getTitle()}</h2>
                    <p><strong>${model.event.getDate()}</strong> - ${model.event.getTitle()}</p>
                </section>
                <section class="3u 12u(medium)">
                    <h3>Информация</h3>
                    <p>${model.event.getDescription()}</p>
                </section>
            </div>
        </header>
        <#else>
            <p>К сожалению, в базе нет такого мероприятия</p>
        </header>
        </#if>
    </div>
</section>

<footer id="footer">
    <div class="inner">
        <#include "common/copyright.ftl">
    </div>
</footer>

<#include "common/scripts.ftl">

</body>

</html>