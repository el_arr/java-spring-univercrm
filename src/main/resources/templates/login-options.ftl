<!DOCTYPE HTML>
<html>

<head>
    <title>Choose your destiny!</title>
    <#include "common/head.ftl">
</head>

<body class="subpage">

<#include "common/header.ftl">

<section id="banner">
    <div class="inner">

        <header>
            <h1>Welcome to 2ка</h1>
        </header>

        <div class="flex">
            <div>
                <a href="/login" class="button big special">Войти</a>
            </div>

            <div>
                <a href="/register" class="button big special">Зарегистрироваться</a>
            </div>
        </div>

        <footer>
            <a href="/" class="button">Перейти к просмотру</a>
        </footer>

    </div>
</section>

<footer id="footer">
    <div class="inner">
        <#include "common/copyright.ftl">
    </div>
</footer>

<#include "common/scripts.ftl">

</body>

</html>
