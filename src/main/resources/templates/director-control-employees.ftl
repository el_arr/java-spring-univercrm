<!DOCTYPE HTML>
<html>

<head>
    <title>Сотрудники</title>
    <#include "common/head.ftl">
</head>

<body class="subpage">

<#include "common/header.ftl">

<section id="main" class="wrapper">
    <div class="inner">

        <header class="align-center">
            <h2>Сотрудники</h2>
    <#if model.employees?size != 0>
            <p>Ниже представлены все сотрудники, обитающие в 2ке</p>
        </header>

        <h2 id="content">Информация</h2>
        <hr class="major"/>

        <#list model.employees as employee>
        <section class="4u 12u$(medium)">
            <h2>${employee.getUser().getFirstName()} ${employee.getUser().getSecondName()}</h2>
            <p>${employee.getJob_description()}</p>
        </section>
        <section class="4u 12u$(medium)">
            <h3>Контактная информация</h3>
            <p>Контакты: ${employee.getUser().getContact()}</p>
            <p>Факультет:
            <#if employee.getDepartment()??>
                <a href="/department/${employee.getDepartment().getId()}">
                    ${employee.getDepartment().getName()}
                </a>
            <#else>
                Сотрудник не закреплён за каким-либо факультетом
            </#if>
            </p>
        </section>
        <section class="4u$ 12u$(medium)">
            <h3>Узнать больше:</h3>
            <a href="/employee/${employee.getId()}" class="button">${employee.getUser().getSecondName()}</a>
        </section>
        <hr class="major"/>
        </#list>
    <#else>
            <p>К сожалению, в базе данных нет видов факультетов, обитающих в 2ке</p>
        </header>
    </#if>
    </div>
</section>

<footer id="footer">
    <div class="inner">
        <#include "common/copyright.ftl">
    </div>
</footer>

<#include "common/scripts.ftl">

</body>

</html>