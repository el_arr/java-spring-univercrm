<!DOCTYPE HTML>
<html>

<head>
    <title>Абитуриент</title>
    <#include "common/head.ftl">
</head>

<body class="subpage">

<#include "common/header.ftl">

<section id="three" class="wrapper">
    <div class="inner">

        <header class="align-center">
            <h2>${model.enrollee.getUser().getFirstName()} ${model.enrollee.getUser().getSecondName()}</h2>
            <p>Абитуриент</p>
        </header>

        <div class="image round right">
            <img src="/img/pic02.jpg" alt="Pic"/>
        </div>

        <h4>Данные</h4>
        <dl>
            <#if model.meme??>
                <#if model.meme == true>
            <dt>Доступ</dt>
            <dd>
                <p>${model.enrollee.getUser().getRole()}</p>
            </dd>
                </#if>
            </#if>
            <dt>Имя</dt>
            <dd>
                <p>${model.enrollee.getUser().getFirstName()}</p>
            </dd>
            <dt>Фамилия</dt>
            <dd>
                <p>${model.enrollee.getUser().getSecondName()}</p>
            </dd>
            <dt>Возраст</dt>
            <dd>
                <p>${model.enrollee.getUser().getAge()}</p>
            </dd>
            <dt>Почтовый ящик</dt>
            <dd>
                <p>${model.enrollee.getUser().getContact()}</p>
            </dd>
            <dt>Диплом</dt>
            <dd>
                <div class="box alt">
                    <div class="row uniform">
                        <div class="4u"><span class="image fit">
                            <img src="/enrollee/${model.enrollee.getId()}/diploma" alt=""/></span>
                        </div>
                    </div>
                </div>
            </dd>
        </dl>

        <header class="align-center">
            <div class="row">
            <#if model.meme??>
            <#if model.meme == true>
                <div class="6u 12u$(small)">
                    <a href="/enrollee/${model.enrollee.getId()}/edit" class="button special">Изменить свои данные</a>
                </div>
                <div class="6u 12u$(small)">
                    <a href="/ticket/enrollee/${model.enrollee.getId()}" class="button special">Мои заявки</a>
                </div>
            </#if>
            </#if>
            </div>
        </header>

    </div>
</section>

<footer id="footer">
    <div class="inner">
        <#include "common/copyright.ftl">
    </div>
</footer>

<#include "common/scripts.ftl">

</body>

</html>
