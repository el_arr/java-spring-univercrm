<!DOCTYPE HTML>
<html>

<head>
    <#if model.speciality??>
    <title>${model.speciality.getName()}</title>
    <#else>
    <title>Специальности. Ошибочка вышла.</title>
    </#if>
    <#include "common/head.ftl">
</head>

<body class="subpage">

<#include "common/header.ftl">

<section id="main" class="wrapper">
    <div class="inner">

        <header class="align-center">
        <#if model.speciality??>
            <h2>${model.speciality.getName()}</h2>
            <p>Ниже представлена информация о данной специальности</p>
        </header>

        <h2 id="content">Информация</h2>
        <p>${model.speciality.getMain_description()}</p>
        <div class="row">
            <div class="6u 12u$(small)">
                <h3>Кем вы станете?</h3>
                <p>${model.speciality.getSmall_description_1()}</p>
            </div>
            <div class="6u$ 12u$(small)">
                <h3>Что будете изучать?</h3>
                <p>${model.speciality.getSmall_description_2()}</p>
            </div>
        </div>

        <hr class="major"/>

        <section class="3u$ 6u$(medium)">
            <a href="/speciality/${model.speciality.getId()}/download" class="button special">Сохранить
                информацию</a>
        </section>

        <hr class="major"/>

        <header class="align-center">
            <h2>Хотите присоединиться?</h2>
            <p><a href="/user" class="button special big">Подать заявку!</a></p>
        </header>
        <#else>
            <h2>Извините, о данной специальности нет никакой информации.</h2>
            <p>Попробуйте поискать еще.</p>
        </header>
        </#if>
    </div>
</section>

<footer id="footer">
    <div class="inner">
        <#include "common/copyright.ftl">
    </div>
</footer>

<#include "common/scripts.ftl">

</body>

</html>
