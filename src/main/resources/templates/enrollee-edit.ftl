<!DOCTYPE HTML>
<html>

<head>
    <title>Абитуриент</title>
    <#include "common/head.ftl">
</head>

<body class="subpage">

<#include "common/header.ftl">

<section id="three" class="wrapper">
    <div class="inner">

        <header class="align-center">
            <h2>Имя Фамилия</h2>
            <p>Абитуриент</p>
        </header>

        <div class="image round right">
            <img src="/img/pic02.jpg" alt="Pic 02"/>
        </div>

        <h4>Данные</h4>
        <dl>
            <dt>Доступ</dt>
            <dd>
                <p>${model.enrollee.getUser().getRole()}</p>
            </dd>
            <dt>Имя</dt>
            <dd>
                <p>${model.enrollee.getUser().getFirstName()}</p>
            </dd>
            <dt>Фамилия</dt>
            <dd>
                <p>${model.enrollee.getUser().getSecondName()}</p>
            </dd>
            <dt>Возраст</dt>
            <dd>
                <p>${model.enrollee.getUser().getAge()}</p>
            </dd>
            <dt>Почтовый ящик</dt>
            <dd>
                <p>${model.enrollee.getUser().getContact()}</p>
            </dd>
        </dl>

        <form enctype="multipart/form-data" class="form-control" method="post" role="form"
              action="/enrollee/${model.enrollee.getId()}/edit-successful">
            <dl>
                <dt>Диплом</dt>
                <dd>
                    <div class="form-group">
                        <label for="file">Добавить диплом</label>
                        <input accept="image/png,image/jpeg" id="file" type="file" name="file">
                    </div>
                    <br/>
                </dd>
                <dd>
                    <ul class="actions">
                        <li><input value="Добавить" class="button special" type="submit"></li>
                    </ul>
                </dd>
            </dl>
        </form>

    </div>
</section>

<footer id="footer">
    <div class="inner">
        <#include "common/copyright.ftl">
    </div>
</footer>

<#include "common/scripts.ftl">

</body>

</html>
