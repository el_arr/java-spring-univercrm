<section class="align-center">
    <#if model.events?size != 0>
        <h2>Мероприятия</h2>

        <div class="slideshow-container">
        <#assign count = 1>
        <#list model.events as event>
            <div class="mySlides fade">
                <img src="img/kpfu_${count}.jpg" width="1024" height="512">
                <h2><a href="/event/${event.getId()}">${event.getTitle()}, ${event.getDate()}</a></h2>
                <h3>${event.getDescription()}</h3>
            </div>
            <#if count == 1>
                <#assign count = 2>
            <#elseif count == 2>
                <#assign count = 1>
            </#if>
        </#list>
        </div>
        <br>
        <div style="text-align:center">
            <span class="dot"></span>
            <span class="dot"></span>
            <span class="dot"></span>
        </div>
        <script>
            var slideIndex = 0;
            showSlides();

            function showSlides() {
                var i;
                var slides = document.getElementsByClassName("mySlides");
                var dots = document.getElementsByClassName("dot");
                for (i = 0; i < slides.length; i++) {
                    slides[i].style.display = "none";
                }
                slideIndex++;
                if (slideIndex > slides.length) {
                    slideIndex = 1
                }
                for (i = 0; i < dots.length; i++) {
                    dots[i].className = dots[i].className.replace(" active", "");
                }
                slides[slideIndex - 1].style.display = "block";
                dots[slideIndex - 1].className += " active";
                setTimeout(showSlides, 5000); // Change image every 5 seconds
            }
        </script>

    </#if>
</section>