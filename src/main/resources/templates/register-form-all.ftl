<!DOCTYPE HTML>
<html>

<head>
    <title>Регистрация</title>
    <#include "common/head.ftl">
</head>

<body class="subpage">

<#include "common/header.ftl">

<footer id="footer">
    <div class="inner">

        <h3>Регистрация</h3>

        <div class="12u$">
            <div class="select-wrapper">
                <select name="role" id="role" form="register">
                    <option value="1">Абитуриент</option>
                    <option value="2">Сотрудник</option>
                    <option value="3">Директор</option>
                    <option value="4">Администратор</option>
                </select>
            </div>
        </div>
        <br/>
        <form action="/register" method="post" role="form" id="register">
            <div class="field half first">
                <label for="username">Имя пользователя</label>
                <input name="username" id="username" type="text" placeholder="Имя пользователя">
            </div>
            <div class="field half">
                <label for="password">Пароль</label>
                <input name="password" id="password" type="password" placeholder="Пароль">
            </div>

            <div class="field half first">
                <label for="first_name">Имя</label>
                <input name="first_name" id="first_name" type="text" placeholder="Имя">
            </div>
            <div class="field half">
                <label for="second_name">Фамилия</label>
                <input name="second_name" id="second_name" type="text" placeholder="Фамилия">
            </div>

            <div class="field half first">
                <label for="age">Возраст</label>
                <input name="age" id="age" type="text" placeholder="Возраст">
            </div>
            <div class="field half">
                <label for="contact">Почтовый ящик</label>
                <input name="contact" id="contact" type="email" placeholder="Почтовый ящик">
            </div>
            <ul class="actions">
                <li><input value="Поехали!" class="button alt" type="submit"></li>
            </ul>
        </form>

        <#include "common/copyright.ftl">

    </div>
</footer>

<#include "common/scripts.ftl">

</body>

</html>
