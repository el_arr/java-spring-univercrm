CREATE TABLE persistent_logins (
  username VARCHAR(64) NOT NULL,
  series VARCHAR(64) NOT NULL,
  token VARCHAR(64) NOT NULL,
  last_used TIMESTAMP NOT NULL,
  PRIMARY KEY (series)
);

INSERT INTO "public"."department" ("id", "description", "email", "phone_num", "name")
VALUES (1, 'Высшая школа информационных технологий и информационных систем (ИТИС) -  инновационный ИТ-факультет КФУ',
'itis@kpfu.ru', '+7(843)221-34-33', 'ВШИТиИС')
INSERT INTO "public"."department" ("id", "description", "email", "phone_num", "name")
VALUES (2, 'Институт вычислительной математики и информационных технологий КФУ',
'vmk.dep@kpfu.ru', '+7(843)233-70-37', 'ИВМиИТ-ВМК')
INSERT INTO "public"."department" ("id", "description", "email", "phone_num", "name")
VALUES (3, 'Институт социально-философских наук и массовых коммуникаций',
'isfnimk@kpfu.ru', '+7(843)233-72-57', 'ИСФНиМК')

INSERT INTO "public"."user" ("id", "age", "contact", "first_name", "role", "second_name")
VALUES (1, 53, '+7(843)233-74-03', 'Ильшат', 'DIRECTOR', 'Гафуров')
INSERT INTO "public"."user" ("id", "age", "contact", "first_name", "role", "second_name")
VALUES (2, 35, '+7(843)221-34-33', 'Айрат', 'DIRECTOR', 'Хасьянов')
INSERT INTO "public"."user" ("id", "age", "contact", "first_name", "role", "second_name")
VALUES (3, 18, 'admin@sobaka.ru', 'admin', 'ADMIN', 'admin')

INSERT INTO "public"."employee" ("id", "job_description", "department_id", "user_id")
VALUES (2, 'Директор топового факультета двойки - ИТИС. Поговаривают, что он круче, чем ректор КАИ.', 1, 2)
INSERT INTO "public"."employee" ("id", "job_description", "department_id", "user_id")
VALUES (3, 'Admin', 1, 3)

INSERT INTO "public"."event" ("id", "date", "description", "title")
VALUES (1, '2018-06-16', 'Экзамен по Информатике', 'Экзамен по Информатике')
INSERT INTO "public"."event" ("id", "date", "description", "title")
VALUES (2, '2018-06-12', 'Экзамен по ВССиТ', 'Экзамен по ВССиТ')
INSERT INTO "public"."event" ("id", "date", "description", "title")
VALUES (3, '2018-06-08', 'Экзамен по Английскому', 'Экзамен по Английскому')

INSERT INTO "public"."userlogindata" ("id", "password_hashed", "role", "status", "username", "related_user_id")
VALUES (1, '$2a$07$GyzFQXSYKpMCGCl6wczRa.fU3YQk8AE3TNRaGmGMeMVUuB28EtfoO', 'ADMIN', 'CONFIRMED', 'admin', 3)
INSERT INTO "public"."userlogindata" ("id", "password_hashed", "role", "status", "username", "related_user_id")
VALUES (3, '$2a$07$syIoO5hWsSjRKCnfgUYmk.7CRw.bKJJBuFLKGmjsJBK21NsQr/oei', 'DIRECTOR', 'CONFIRMED', 'gafurov', 1)
INSERT INTO "public"."userlogindata" ("id", "password_hashed", "role", "status", "username", "related_user_id")
VALUES (4, '$2a$07$8HGaenps2sq31nKXb3lF9OdAQSqwiB/YwZBeSRHkfo0DD4aetWVku', 'DIRECTOR', 'CONFIRMED', 'khasianov', 2)

INSERT INTO "public"."speciality"
("id", "main_description", "small_description_1", "small_description_2", "department_id", "name")
VALUES
(1, 'Направления обучения:
Инт.Робототехника
Маш. понимание, выч. эмоции
Виз., 3D-мод., разр. компьютерных игр
Моб. разр. (Android, iOS)
Web-разр.
Разр. корп. реш. (Java, .NET, 1C)
Инфрастр. решения
Инт. обуч. системы',
'Вы сможете пойти в любую сферу после окончания соответствующего курса.',
'Вам будут предложены курсы,специализирующиеся на одной из тематик,названных выше.', 1,
'Программная инженерия')
INSERT INTO "public"."speciality"
("id", "main_description", "small_description_1", "small_description_2", "department_id", "name")
 VALUES
 (2, 'Бизнес-информатика - этим всё сказано',
 'Вы вряд-ли что-то сможете после окончания курса. Ну, разве что быть менеджером.',
'Вас научат как быть менеджером (Сомнительная перспектива).', 2, 'Бизнес-информатика')